# GoBear

Requirements ( 6 Screens ):

Launch / App walkthrough ( 3 Screens )
Feature(s):
	Skip walkthrough button.
	
Login ( 1 Screen )
Feature(s):
	Username.
	Password.
	Remember me call to action.
	Field validation.
	Dummy credentials (No api). 
	Username: GoBear
	Password : GoBearDemo
	Store locally.
	
News list ( 1 Screen )
Feature(s):
	Consumes: 
		http://feeds.bbci.co.uk/news/world/asia/rss.xml
	ListView
		Article title.
		Article date.
		Article small Image.
		Article story intro.
		Pull to refresh.
	Logout call to action.
	
News detail ( 1 Screen )
Feature(s):
	Article title.
	Article date.
	Article full image.
	Article full story.