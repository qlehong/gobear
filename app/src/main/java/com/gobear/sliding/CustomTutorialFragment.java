package com.gobear.sliding;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.cleveroad.slidingtutorial.IndicatorOptions;
import com.cleveroad.slidingtutorial.OnTutorialPageChangeListener;
import com.cleveroad.slidingtutorial.TutorialFragment;
import com.cleveroad.slidingtutorial.TutorialOptions;
import com.cleveroad.slidingtutorial.TutorialPageProvider;
import com.gobear.LoginActivity;
import com.gobear.R;

public class CustomTutorialFragment extends TutorialFragment
        implements OnTutorialPageChangeListener {

    private static final String TAG = "CustomTutorialFragment";
    private static final int TOTAL_PAGES = 3;

    private final View.OnClickListener mOnSkipClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            gotoLoginScreen();
        }
    };

    private void gotoLoginScreen() {
        Intent intent = new Intent(getActivity(), LoginActivity.class);
        getActivity().startActivity(intent);
        getActivity().finish();
    }

    private final TutorialPageProvider<Fragment> mTutorialPageProvider = new CustomTutorialPageProvider();

    private int[] pagesColors;

    public static CustomTutorialFragment newInstance() {
        Bundle args = new Bundle();
        CustomTutorialFragment fragment = new CustomTutorialFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (pagesColors == null) {
            Activity activity = getActivity();
            pagesColors = new int[]{
                    ContextCompat.getColor(activity, android.R.color.darker_gray),
                    ContextCompat.getColor(activity, android.R.color.holo_green_dark),
                    ContextCompat.getColor(activity, android.R.color.holo_red_dark),
                    ContextCompat.getColor(activity, android.R.color.holo_blue_dark),
                    ContextCompat.getColor(activity, android.R.color.holo_purple),
                    ContextCompat.getColor(activity, android.R.color.holo_orange_dark),
            };
        }
        addOnTutorialPageChangeListener(this);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.custom_tutorial_layout_ids_example;
    }

    @Override
    protected int getIndicatorResId() {
        return R.id.indicatorCustom;
    }

    @Override
    protected int getSeparatorResId() {
        return R.id.separatorCustom;
    }

    @Override
    protected int getButtonSkipResId() {
        return R.id.tvSkipCustom;
    }

    @Override
    protected int getViewPagerResId() {
        return R.id.viewPagerCustom;
    }

    @Override
    protected TutorialOptions provideTutorialOptions() {
        return newTutorialOptionsBuilder(getActivity())
                .setUseAutoRemoveTutorialFragment(true)
                .setUseInfiniteScroll(true)
                .setShowSkipButton(true)
                .setPagesColors(pagesColors)
                .setPagesCount(TOTAL_PAGES)
                .setIndicatorOptions(IndicatorOptions.newBuilder(getActivity())
                        .setElementSizeRes(R.dimen.indicator_size)
                        .setElementSpacingRes(R.dimen.indicator_spacing)
                        .setElementColorRes(android.R.color.darker_gray)
                        .setSelectedElementColor(Color.LTGRAY)
                        .setRenderer(DrawableRenderer.create(getActivity()))
                        .build())
                //.setTutorialPageProvider(mTutorialPageOptionsProvider)
                .setTutorialPageProvider(mTutorialPageProvider)
                .setOnSkipClickListener(mOnSkipClickListener)
                .build();
    }

    @Override
    public void onPageChanged(int position) {
        if (position == 0) {
            gotoLoginScreen();
        }
    }

}
