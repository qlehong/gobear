package com.gobear;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.gobear.sliding.CustomTutorialFragment;

public class SlidingActivity extends AppCompatActivity {

    public static final String KEY_ROLLBACK = "key_rollback";

    public static void start(Context context) {
        start(context, false);
    }

    public static void start(Context context, boolean noRollback) {
        Intent intent = new Intent(context, SlidingActivity.class);
        intent.putExtra(KEY_ROLLBACK, noRollback);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sliding);

        if (savedInstanceState == null) {
            replaceTutorialFragment();
        }
    }

    public void replaceTutorialFragment() {
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.container, CustomTutorialFragment.newInstance())
                .commit();
    }

}
